<?php

    require_once(dirname(__FILE__) . '/config.inc.php');

?>

<!DOCTYPE html>
<html>
    <head>
        <title>D&eacute;brideur</title>

        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png"/>

        <!-- Styles -->
        <link type='text/css' rel='stylesheet' href='includes/bootstrap/css/bootstrap.min.css' />
        <link type='text/css' rel='stylesheet' href='includes/font-awesome-4.7.0/css/font-awesome.min.css' />
        <link type='text/css' rel='stylesheet' href='style.css' />

    </head>
    <body>
        <div class='container'>
            <div class='alert alert-danger'>
                Des erreurs dans votre configuration :
                <ul>
                    <?= $configErrors ?>
                </ul>
                <br />
                Votre fichier de configuration : <?= _CONFIG_JSONFILE ?>
            </div>
            
            <?php require_once(dirname(__FILE__) . '/configForm.php'); ?>
        </div>
    </body>
</html>