<?php

    require_once(dirname(__FILE__) . '/config.inc.php');

?>

<form method='POST'>
    <input type='hidden' name='mode' value='save_config' />
    
    <section>
        <h2>G&eacute;n&eacute;ral</h2>
        <div class="form-group">
            <label for="t_newPrefixPackage">Pr&eacute;fix des packages</label>
            <div class="input-group">
                <input type="text" class="form-control" id="t_newPrefixPackage" placeholder="Nouveau pr&eacute;fix" />
                <span class="input-group-btn">
                    <button type='button' class='btn btn-default' id='b_newPrefixPackage'><i class='fa fa-plus text-success'></i></button>
                </span>
            </div>
            <ul id='listPrefixPackage'>
            <?php
            
                foreach ($CONFIG['package_prefix'] as $prefix)
                {
                    echo "<li><input type='hidden' name='package_prefix[]' value=\"".$prefix."\" /><i class='fa fa-trash text-danger clickable prefixPackageRemove' aria-hidden='true'></i> ".$prefix."</li>";
                }
                
            ?>
            </ul>
        </div>
        <div class="form-group">
            <label for="t_prefixPackageSeparator">S&eacute;parateur des pr&eacute;fixes</label>
            <input type="text" class="form-control" id="t_prefixPackageSeparator" name='prefixpackage_separator' placeholder="S&eacute;parateur" value="<?= $CONFIG['prefixpackage_separator'] ?>" />
        </div>
    </section>
    
    <section>
        <h2>Th&egrave;me</h2>
        <div class="form-group">
            <label for="s_topnavbarstyle">Style de la barre de navigation du haut</label>
            <select id='s_topnavbarstyle' name='topnavbar_style' class="form-control">
            <?php

                foreach ($NAVBAR_STYLES as $key => $color)
                {
                    $selected = '';
                    
                    if ($CONFIG['topnavbar_style'] == $key)
                    {
                         $selected = "selected='selected'";
                    }
                    
                    echo "<option value=\"".$key."\" ".$selected.">".$color."</option>\n";
                }

            ?>
            </select>
        </div>
    </section>
    
    <section>
        <h2>pyLoad</h2>
        <div class="form-group">
            <label for="t_pyLoadApi">API pyLoad</label>
            <input type="text" class="form-control"  id="t_pyLoadApi" name="pyload_api" placeholder="URL de l'API pyLoad" value="<?= $CONFIG['pyload_api'] ?>" />
        </div>
        <div class="form-group">
            <label for="t_pyLoadUser">Nom d'utilisateur</label>
            <input type="text" class="form-control" id="t_pyLoadUser" name="pyload_user" placeholder="Nom d'utilisateur pyLoad" value="<?= $CONFIG['pyload_user'] ?>" />
        </div>
        <div class="form-group">
            <label for="p_pyLoadPass">Mot de passe</label>
            <input type="password" class="form-control" id="p_pyLoadPass" name="pyload_pass" placeholder="Mot de passe pyLoad" value="<?= $CONFIG['pyload_pass'] ?>" />
        </div>
    </section>
    
    <section>
        <h2>AllDebrid</h2>
        <div class="form-group">
            <label for="t_allDebridToken">Token</label>
            <input type="text" class="form-control" id="t_allDebridToken" name="debrid_token" placeholder="Token AllDebrid" value="<?= $CONFIG['debrid_token'] ?>" />
        </div>
    </section>
    
    <div class="form-group text-right">
        <button class='btn btn-danger' type='reset'><i class='fa fa-times' aria-hidden='true'></i> R&eacute;initialiser</button>
        <button class='btn btn-primary' type='submit'><i class='fa fa-save' aria-hidden='true'></i> Enregistrer</button>
    </div>
</form>