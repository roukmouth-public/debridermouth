<?php

if ($_SERVER['HTTPS'] == 'on')
{
    define ('_URL_ROOT', 'https://'.$_SERVER['HTTP_HOST']);
}
else
{
    define ('_URL_ROOT', 'http://'.$_SERVER['HTTP_HOST']);
}

$configFields = $CONFIG = array();

# configuration par défaut
$_DEFAULT_PREFIXPACKAGE = array();
define ('_DEFAULT_TOPNAVBARSTYLE', 'default');
define ('_DEFAULT_PREFIXPACKAGE_SEPARATOR', ' ');
define ('_DEFAULT_PYLOAD_API', 'http://localhost:8000/api/');
define ('_DEFAULT_PYLOAD_USER', '');
define ('_DEFAULT_PYLOAD_PASS', '');
define ('_DEFAULT_DEBRID_TOKEN', '');

$configFields['topnavbar_style'] = _DEFAULT_TOPNAVBARSTYLE;
$configFields['package_prefix'] = $_DEFAULT_PREFIXPACKAGE;
$configFields['prefixpackage_separator'] = _DEFAULT_PREFIXPACKAGE_SEPARATOR;
$configFields['pyload_api'] = _DEFAULT_PYLOAD_API;
$configFields['pyload_user'] = _DEFAULT_PYLOAD_USER;
$configFields['pyload_pass'] = _DEFAULT_PYLOAD_PASS;
$configFields['debrid_token'] = _DEFAULT_DEBRID_TOKEN;

define ('_COOKIES_PATH', dirname(__FILE__) . '/../connectors/cookies/');
define ('_ICONS_PATH', dirname(__FILE__) . '/../images/icons/');
define ('_CONFIG_JSONFILE', dirname(__FILE__) . '/config.json');
define ('_URL_JSONFILE', _URL_ROOT.'/config/config.json');

$NAVBAR_STYLES = array('default' => 'Gris', 'inverse' => 'Noir');

// Teste de l'accessibilité d'une URL
function urlAccess($url)
{
    $headers = @get_headers($url);
    
    if (strpos($headers[0], '200') === false)
    {
        return false;
    }
    else
    {
        return true;
    }
}

// Lecture de la configuration
function readConfig()
{
    global $PACKAGE_PREFIX, $CONFIG, $configFields;
    
    if (is_readable(_CONFIG_JSONFILE))
    {
        if ($content = file_get_contents(_CONFIG_JSONFILE))
        {
            if (function_exists('json_decode'))
            {
                $json = json_decode($content);
                
                foreach ($configFields as $key => $val)
                {
                    if (isset($json->{$key}))
                    {
                        $CONFIG[$key] = $json->{$key};
                    }
                }
            }
            else
            {
                echo "Les fonctions JSON ne sont pas impl&eacute;ment&eacute;es sur votre serveur PHP.";

                exit;
            }
        }
        else
        {
             echo "impossible de lire le fichier de configuration : "._CONFIG_JSONFILE;

             exit;
        }
    }
}

// Enregistrement de la configuration
function writeConfig($settings)
{
    global $configFields;
    
    if (!function_exists('json_decode'))
    {
        echo "Les fonctions JSON ne sont pas impl&eacute;ment&eacute;es sur votre serveur PHP.";

        exit;
    }
    
    $json = '';
    
    foreach ($configFields as $key => $val)
    {
        if (!in_array($key, $settings))
        {
            if ($json != '')
            {
                $json .= ",\n";
            }

            $json .= '"'.$key.'": '.json_encode($settings[$key]);
        }
        else
        {
            return false;
        }
    }
    
    $json = "{\n" . $json ."\n}";
    
    // Ouverture du fichier
    if ($fichier = fopen(_CONFIG_JSONFILE, 'w+'))
    {
        // Ecriture dans le fichier
        if (fwrite($fichier, $json))
        {
            // Fermeture du fichier
            if (fclose($fichier))
            {
                return true;
            }
        }
    }
    
    return false;
}

// Lecture de la configuration
if (is_file(_CONFIG_JSONFILE))
{
    readConfig();
}
// Si la configuration n'existe pas
else
{
    // Création de la configuration avec les valeurs par défaut
    if (writeConfig($configFields))
    {
        // Lecture de la configuration
        readConfig();
    }
}

// Modification de la configuration
if ($_POST['mode'] == 'save_config')
{
    $settings = array();
    
    foreach ($configFields as $key => $val)
    {
        $settings[$key] = $_POST[$key];
    }
    
    if (writeConfig($settings))
    {
        require_once('configSave.php');

        header("Refresh:3; url=/", true, 303);
    }
    else
    {
        $configErrors = "<li>Une erreur s'est produite lors de l'enregistrement de votre configuration.</li>";
        
        require_once('configError.php');

        header("Refresh:3; url=/", true, 303);
    }
    
    exit;
}

// Vérification de la configuration
$configErrors = '';

/*echo '<pre>';
print_r($NAVBAR_STYLES);
print_r($CONFIG);
echo '</pre>';*/

if (urlAccess(_URL_JSONFILE))
{
     $configErrors .= "<li>Votre fichier de configuration est accessible &agrave; tous via <a href=\""._URL_JSONFILE."\" target='_blank'>"._URL_JSONFILE."</a>, veuillez le prot&eacute;ger.</li>\n";
}

if (!function_exists('json_decode'))
{
    $configErrors .= "<li>Les fonctions JSON ne sont pas impl&eacute;ment&eacute;es sur votre serveur PHP. Veuillez mettre &agrave; jour votre version de PHP.</li>\n";
}

if (!is_writable(_COOKIES_PATH))
{
     $configErrors .= "<li>Le r&eacute;pertoire des cookies n'est pas accessible en &eacute;criture : <div class='well'>sudo chmod 777 "._COOKIES_PATH."</div></li>\n";
}

if (!array_key_exists('topnavbar_style', $CONFIG) || !array_key_exists($CONFIG['topnavbar_style'], $NAVBAR_STYLES))
{
    $configErrors .= "<li>Veuillez choisir un style pour la barre de navigation du haut.</li>\n";
}

if (!array_key_exists('pyload_api', $CONFIG) || $CONFIG['pyload_api'] == '')
{
    $configErrors .= "<li>Veuillez renseigner l'adresse de l'API pyLoad.</li>\n";
}

if (!array_key_exists('pyload_user', $CONFIG) || $CONFIG['pyload_user'] == '')
{
    $configErrors .= "<li>Veuillez renseigner le nom d'utilisateur pour la connexion pyLoad.</li>\n";
}

if (!array_key_exists('pyload_pass', $CONFIG) || $CONFIG['pyload_pass'] == '')
{
    $configErrors .= "<li>Veuillez renseigner le mot de passe pour la connexion pyLoad.</li>\n";
}

if (!array_key_exists('debrid_token', $CONFIG) || $CONFIG['debrid_token'] == '')
{
    $configErrors .= "<li>Veuillez renseigner le token pour la connexion AllDebrid.</li>\n";
}

if ($configErrors != '')
{
    http_response_code(500);
    
    require_once('configError.php');
    
    exit;
}