<!DOCTYPE html>
<html>
    <head>
        <title>D&eacute;brideur</title>

        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png"/>

        <!-- Styles -->
        <link type='text/css' rel='stylesheet' href='includes/bootstrap/css/bootstrap.min.css' />
        <link type='text/css' rel='stylesheet' href='includes/font-awesome-4.7.0/css/font-awesome.min.css' />
        <link type='text/css' rel='stylesheet' href='style.css' />

    </head>
    <body>
        <div class='container'>
            <div class='alert alert-success'>
                <i class='fa fa-check' aria-hidden='true'></i> Votre configuration a &eacute;t&eacute; sauvegard&eacute;e avec succ&egrave;s !<br />
                <br />
                Vous allez &ecirc;tre redirig&eacute; dans quelques secondes...
            </div>
        </div>
    </body>
</html>