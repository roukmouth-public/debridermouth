# Description

DebriderMouth est une interface web (PHP + JS) pour débrider des liens (uptobox.com, 1fichier.com, uploaded.to, etc...) avec AllDebrid.com et ajouter les liens débridés sur pyLoad.


# Téléchargement

```shell
git clone git@framagit.org:roukmouth-public/debridermouth.git /var/www/debrider
```


# Paramétrage

```shell
sudo chown -R www-data:www-data /var/www/debrider/
sudo chmod 777 /var/www/debrider/connectors/cookies/
```


# Modules PHP

Afin de permettre les connexions vers AllDebrid.com et PyLoad, l'application a besoin de cURL pour PHP.

```shell
# Connaître la version de PHP installé
php --version
PHP 7.0.33-0+deb9u10...

# Pour PHP5
sudo apt-get update && sudo apt-get install php5-curl

# Pour PHP7.0
sudo apt-get update && sudo apt-get install php7.0-curl
```


# Configuration

## AllDebrid.com

Afin de requêter l'API d'Alldebrid.com, veuillez vous rendre à cette adresse : https://alldebrid.fr/apikeys/

Créez une nouvelle clé portant le nom de **debridermouth** et récupérez l'ApiKey correspondante. Elle vous sera demandée dans le panneau de configuration de DebriderMouth.


## DebriderMouth

A l'ouverture de l'application, votre fichier de configuration **/var/www/debrider/config/config.json** sera créé automatiquement.

Suivez les instructions à l'écran afin de renseigner les informations nécessaires au fonctionnement de cette application.


# Sécurisation

## Installation de apache2-utils

```shell
sudo apt-get install apache2-utils
```


## Création du fichier des mots de passe

```shell
sudo htpasswd -c /home/login/.htpasswd/debrid.domaine.tld votrelogin
```
* Remplacez ce chemin par la home de votre compte local par exemple.
* Remplacez également le nom du fichier par celui que vous souhaitez. Votre nom de domaine correspondrait tout à fait.
* Remplacez _votrelogin_ par le nom d'utilisateur souhaité.
* Tapez ensuite votre mot de passe


# Exemples de virtualHost NginX


## En HTTP (port 80)

```
server {
	listen 80;
	server_name debrid.domaine.tld;

	root /var/www/debrider;
	index index.php;

	disable_symlinks off;

	access_log /var/log/nginx/debrid.domaine.tld-access.log;
	error_log /var/log/nginx/debrid.domaine.tld-error.log;

	location ~ \.php$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/var/run/php5-fpm.sock;
		fastcgi_index index.php;
		include fastcgi_params;
	}
	
	location / {
		try_files $uri $uri/ =404;
		auth_basic "Veuillez vous connecter pour acceder au debrider";
		auth_basic_user_file /home/login/.htpasswd/debrid.domaine.tld;
	}
	
	location /config/ {
		allow 127.0.0.1;
		deny all;
	}
}
```


## En HTTPS (port 443)

```
server {
	listen 80;
	server_name debrid.domaine.tld;
	
	## Redirige le HTTP vers le HTTPS ##
	return 301 https://$server_name$request_uri;
}

server {
	listen 443 ssl;
	server_name debrid.domaine.tld;

	root /var/www/debrider;
	index index.php;

	disable_symlinks off;

	access_log /var/log/nginx/debrid.domaine.tld-access.log;
	error_log /var/log/nginx/debrid.domaine.tld-error.log;
	
	ssl on;
	ssl_certificate /etc/nginx/ssl/debrid.domaine.tld.crt-unified;
	ssl_certificate_key /etc/nginx/ssl/debrid.domaine.tld.key;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers "EECDH+AESGCM:AES128+EECDH:AES256+EECDH";

	ssl_prefer_server_ciphers on;
	ssl_ecdh_curve secp384r1;

	ssl_session_cache   shared:SSL:10m;
	ssl_session_timeout 10m;

	ssl_stapling        on;
	ssl_stapling_verify on;

	resolver 8.8.4.4 8.8.8.8 valid=300s;
	resolver_timeout 10s;

	add_header X-Frame-Options "DENY";
	add_header X-Content-Type-Options "nosniff";

	location ~ \.php$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/var/run/php5-fpm.sock;
		fastcgi_index index.php;
		include fastcgi_params;
	}
	
	location / {
		try_files $uri $uri/ =404;
		auth_basic "Veuillez vous connecter pour acceder au debrider";
		auth_basic_user_file /home/login/.htpasswd/debrid.domaine.tld;
	}
	
	location /config/ {
		allow 127.0.0.1;
		deny all;
	}
}
```


# Mettre à jour l'application

```shell
cd /var/www/debrider/
git pull
```
