<?php

require_once('config/config.inc.php');

$preventCache = '?preventCache='. time();

?>

<!DOCTYPE html>
<html>
	<head>
		<title>D&eacute;brideur</title>
		
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png"/>

		<!-- Styles -->
		<link type='text/css' rel='stylesheet' href='includes/bootstrap/css/bootstrap.min.css' />
		<link type='text/css' rel='stylesheet' href='includes/font-awesome-4.7.0/css/font-awesome.min.css' />
		<link type='text/css' rel='stylesheet' href='css/style.css<?= $preventCache ?>' />

	</head>
	<body>
	
		<!-- Top Navbar -->
		<nav class="navbar navbar-<?= $CONFIG['topnavbar_style'] ?> navbar-fixed-top">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="">Debrider</a>
				</div>

				<div class="collapse navbar-collapse" id="navbar-top">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								 <span id='img_conn_allDebrid'><i class="fa fa-times text-danger" aria-hidden="true"></i></span>
								 Connexion AllDebrid
								 <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="#" id='b_debrid_connection'><i class='fa fa-bolt fa-fw' aria-hidden='true'></i> Se reconnecter</a></li>
								<li><a href='#' id='b_debrid_userinfo'><i class='fa fa-user fa-fw' aria-hidden='true'></i> Informations du compte</a></li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								 <span id='img_conn_pyload'><i class="fa fa-times text-danger" aria-hidden="true"></i></span>
								 Connexion pyLoad
								 <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="#" id='b_pyload_connection'><i class='fa fa-bolt fa-fw' aria-hidden='true'></i> Se reconnecter</a></li>
							</ul>
						</li>
					</ul>
					
					<ul class="nav navbar-nav navbar-right">
						<li><a href='#' id='b_config'><i class="fa fa-cogs" aria-hidden="true"></i> Configuration</a></li>
						<li><a href='#' id='b_list_hosters'><i class="fa fa-users" aria-hidden="true"></i> Liste des fournisseurs</a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav>
		
		<div class='container'>
			
			<section class='row'>
				<textarea id='txt_links' class='form-control'></textarea>
				<div style='text-align: right;'>
					<a href="#" class="btn btn-danger" id='b_vider'><span class="fa fa-trash"></span> Vider</a>
					<a href="#" class="btn btn-primary" id='b_debrid'><span class="fa fa-unlink"></span> D&eacute;brider</a>
				</div>
			</section>
			
			<section class='row' id='sect_tab_result'>
				<table id='tab_result' class='table table-bordered'>
					<thead></thead>
					<tbody></tbody>
					<tfoot></tfoot>
				</table>
			</section>
			
			<section class='row' id='sect_links_debrid'>
				<textarea id='txt_links_debrid' class='form-control'></textarea>
				<div class='row form-inline'>
					<div class="col-lg-5 form-group">
						<label for='s_prefixPackage'>Pr&eacute;fix du package :</label>
						<select id='s_prefixPackage' class='form-control'>
							<option value=''>Aucun pr&eacute;fix</option>
							<?php

							if (array_key_exists('package_prefix', $CONFIG) && count($CONFIG['package_prefix']) > 0)
							{
								foreach ($CONFIG['package_prefix'] as $prefix)
								{
									echo "<option value=\"".$prefix.$CONFIG['prefixpackage_separator']."\">".$prefix."</option>";
								}
							}

							?>
						</select>
					</div>
					<div class="col-lg-5 form-group">
						<label for='t_packageName'>Nom du package :</label>
						<input type='text' id='t_packageName' class='form-control' />
					</div>
					<div class="col-lg-2 text-right">
						<a href="#" class="btn btn-primary" id='b_allDownload'><span class="glyphicon glyphicon-download"></span> Tout t&eacute;l&eacute;charger</a>
					</div>
				</div>
			</section>
			
		</div>
		
		<!-- Informations compte debrider -->
		<div id='debrid_userinfo'></div>
		
		<!-- Liste des hosters -->
		<div id='debrid_hostersList'>
			<table class='table table-stripped'>
				<thead>
					<tr>
						<th></th>
						<th>Domaine</th>
						<th>Quota</th>
						<th>Dl simultan&eacute;s</th>
						<th>Statut</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
		
		<!-- Fenêtre modale divers -->
		<div id='modal_divers' class="modal fade">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Fermer"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"></h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- jQuery & jQuery UI -->
		<script type='text/javascript' src='includes/jquery-3.1.1.min.js'></script>
		
		<!-- Bootstrap -->
		<script type='text/javascript' src='includes/bootstrap/js/bootstrap.min.js'></script>
		
		<!-- Scripts -->
		<script type='text/javascript' src='includes/is_int.js<?= $preventCache ?>'></script>
		<script type='text/javascript' src='js/index.js<?= $preventCache ?>'></script>
		<script type='text/javascript' src='connectors/alldebrid.js<?= $preventCache ?>'></script>
		<script type='text/javascript' src='connectors/pyload.js<?= $preventCache ?>'></script>
		<script type='text/javascript' src='js/security.js<?= $preventCache ?>'></script>
		
	</body>
</html>