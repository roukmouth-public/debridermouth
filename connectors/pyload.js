pyloadController = '/connectors/pyload.php'

function connexion_downloader()
{
	downloader_connection_ko();
	
	$('#img_conn_pyload').html(icon_loading);
	
	$.ajax(
	{
		type: 'POST',
		url: pyloadController,
		data: { 'mode': 'pyload_connect' },
		async: false,
		success: function(data)
		{
			//alert(data);
			
			if (data == "CONNECTION_OK")
			{
				$('#img_conn_pyload').html(icon_ok);

				$('#b_allDownload').removeAttr('disabled');
				
				conn_downloader = true;
			}
			else
			{
				downloader_connection_ko();
			}
		},
		error: function()
		{
			downloader_connection_ko();
		}
	});
}

function downloader_addLink(links, packageName)
{
	$.post(pyloadController, { 'mode': 'pyload_addPackage', 'links': links, 'packageName': packageName }, function(data)
	{
		var settings = {};
		
		if (is_int(data))
		{	
			settings.title = "Ajout d'un package dans pyLoad";
			settings.body = "<div class='alert alert-success'><span class='glyphicon glyphicon-ok'></span> Ajout avec succ&egrave;s du package dans pyLoad.</div>";
			settings.footer = "<button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>";
			
			modal_divers(settings);
		}
		else
		{
			settings.title = "Ajout d'un package dans pyLoad";
			settings.body = "<div class='alert alert-danger'><span class='glyphicon glyphicon-ok'></span> "+ data +"</div>";
			settings.footer = "<button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>";
			
			modal_divers(settings);
		}
	});
}