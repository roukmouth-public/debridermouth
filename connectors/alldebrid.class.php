<?php

session_start();

include_once('../config/config.inc.php');

// Doc officielle : https://docs.alldebrid.com/

define ('_DEBRID_URL', 'https://www.alldebrid.com');
define ('_DEBRID_HOSTERS_ICON', _DEBRID_URL.'/lib/images/hosts/');
define ('_DEBRID_COOKIE_FILENAME', __DIR__ . '/cookies/cookie_alldebrid.com.txt');

class AllDebrid
{
	private $_user = '';
	private $_token = '';
	private $_userInfo = '';
	private $_agent = 'debridermouth';
	private $_apiUrl = 'https://api.alldebrid.com/v4';
	private $_apiErrors = [
		'GENERIC' => 'An error occured',
		'404' => "Endpoint doesn't exist",
	
		'AUTH_MISSING_AGENT' => "You must sent a meaningful agent parameter, see api docs",
		'AUTH_BAD_AGENT' => "Bad agent",
		'AUTH_MISSING_APIKEY' => 'The auth apikey was not sent',
		'AUTH_BAD_APIKEY' => 'The auth apikey is invalid',
		'AUTH_BLOCKED' => 'This apikey is geo-blocked or ip-blocked',
		'AUTH_USER_BANNED' => 'This account is banned',
	
		'LINK_IS_MISSING' => 'No link was sent',
		'LINK_HOST_NOT_SUPPORTED' => 'This host or link is not supported',
		'LINK_DOWN' => 'This link is not available on the file hoster website',
		'LINK_PASS_PROTECTED' => 'Link is password protected',
		'LINK_HOST_UNAVAILABLE' => 'Host under maintenance or not available',
		'LINK_TOO_MANY_DOWNLOADS' => 'Too many concurrent downloads for this host',
		'LINK_HOST_FULL' => 'All servers are full for this host, please retry later',
		'LINK_HOST_LIMIT_REACHED' => "You have reached the download limit for this host",
		'LINK_ERROR' => 'Could not unlock this link',
	
		'REDIRECTOR_NOT_SUPPORTED' => 'Redirector not supported',
		'REDIRECTOR_ERROR' => 'Could not extract links',
	
		'STREAM_INVALID_GEN_ID' => 'Invalid generation ID',
		'STREAM_INVALID_STREAM_ID' => 'Invalid stream ID',
	
		'DELAYED_INVALID_ID' => "This delayed link id is invalid",
	
		'FREE_TRIAL_LIMIT_REACHED' => 'You have reached the free trial limit (7 days // 25GB downloaded or host uneligible for free trial)',
		'MUST_BE_PREMIUM' => "You must be premium to process this link",
	
		'MAGNET_INVALID_ID' => 'This magnet ID does not exists or is invalid',
		'MAGNET_INVALID_URI' => "Magnet is not valid",
		'MAGNET_INVALID_FILE' => "File is not a valid torrent",
		'MAGNET_FILE_UPLOAD_FAILED' => "File upload failed",
		'MAGNET_NO_URI' => "No magnet sent",
		'MAGNET_PROCESSING' => "Magnet is processing or completed",
		'MAGNET_TOO_MANY_ACTIVE' => "Already have maximum allowed active magnets (30)",
		'MAGNET_MUST_BE_PREMIUM' => "You must be premium to use this feature",
		'MAGNET_NO_SERVER' => "Server are not allowed to use this feature",
		'MAGNET_TOO_LARGE' => "Magnet files are too large (max 1TB)",
	
		'PIN_ALREADY_AUTHED' => "You already have a valid auth apikey",
		'PIN_EXPIRED' => "The pin is expired",
		'PIN_INVALID' => "The pin is invalid",
	
		'USER_LINK_MISSING' => "No link provided",
		'USER_LINK_INVALID' => "Can't save those links",
	
		'NO_SERVER' => "Server are not allowed to use this feature",
	
		'MISSING_NOTIF_ENDPOINT' => 'You must provide an endpoint to unsubscribe',
	];

	public function __construct($token)
	{
		$this->_token = $token;
		
		$this->login();
	}
	
	private function session_destroy()
	{
		if ($_SESSION['token'] != '')
		{
			session_destroy();
			unset($_SESSION);
		}
	}
	
	private function session_reader()
	{
		//$this->_token = '';
		$this->_userInfo->username = '';
		$this->_userInfo->email = '';
		$this->_userInfo->isPremium = '';
		$this->_userInfo->premiumUntil = '';
		$this->_userInfo->isTrial = '';
		$this->_userInfo->lang = '';
		$this->_userInfo->preferedDomain = '';
		$this->_userInfo->fidelityPoints = '';
		$this->_userInfo->limitedHostersQuotas = '';
		$this->_userInfo->remainingTrialQuota = '';

		//echo 'session_reader : '.$_SESSION['token'].'<br />';

		if (isset($_SESSION['token']) and $_SESSION['token'] != '')
		{
			//$this->_token = $_SESSION['token'];
			$this->_userInfo->username = $_SESSION['username'];
			$this->_userInfo->email = $_SESSION['email'];
			$this->_userInfo->isPremium = $_SESSION['isPremium'];
			$this->_userInfo->premiumUntil = $_SESSION['premiumUntil'];
			$this->_userInfo->isTrial = $_SESSION['isTrial'];
			$this->_userInfo->lang = $_SESSION['lang'];
			$this->_userInfo->preferedDomain = $_SESSION['preferedDomain'];
			$this->_userInfo->fidelityPoints = $_SESSION['fidelityPoints'];
			$this->_userInfo->limitedHostersQuotas = $_SESSION['limitedHostersQuotas'];
			$this->_userInfo->remainingTrialQuota = $_SESSION['remainingTrialQuota'];

			//echo 'session : '.$this->_token.'<br />';
		}
	}

	private function session_writer()
	{
		if ($this->_token != '')
		{
			$_SESSION['token'] = $this->_token;
			$_SESSION['username'] = $this->_userInfo->username;
			$_SESSION['email'] = $this->_userInfo->email;
			$_SESSION['isPremium'] = $this->_userInfo->isPremium;
			$_SESSION['premiumUntil'] = $this->_userInfo->premiumUntil;
			$_SESSION['isTrial'] = $this->_userInfo->isTrial;
			$_SESSION['lang'] = $this->_userInfo->lang;
			$_SESSION['preferedDomain'] = $this->_userInfo->preferedDomain;
			$_SESSION['fidelityPoints'] = $this->_userInfo->fidelityPoints;
			$_SESSION['limitedHostersQuotas'] = $this->_userInfo->limitedHostersQuotas;
			$_SESSION['remainingTrialQuota'] = $this->_userInfo->remainingTrialQuota;
		}
		else
		{
			$this->session_destroy();
		}
	}
	
	/*private function urlAccess($url)
	{
		$headers = @get_headers($url);

		if(strpos($headers[0], '200') === false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private function download_icon($icon)
	{
		if ($this->urlAccess(_DEBRID_HOSTERS_ICON.$icon.'.png'))
			{
				file_put_contents(_ICONS_PATH.$icon.'.png', file_get_contents(_DEBRID_HOSTERS_ICON.$icon.'.png'));
			}
	}*/
	
	public function icon($host)
	{
		switch($host)
		{
			case 'uploaded': case 'ul.to':
				$icon = 'uploadedto';
				break;
			case 'rg':
				$icon = 'rapidgator';
				break;
			case 'load.to':
				$icon = 'loadto';
				break;
			case 'rapidshare.com':
				$icon = 'rs';
				break;
			case 'datei.to':
				$icon = 'dateito';
				break;
			case 'share-rapid.com':
				$icon = 'sharerapid';
				break;
			default:
				$icon = $host;
		}
		
		if (!is_file(_ICONS_PATH.$icon.'.png'))
		{
			return '/images/icons/no_icon.png';
		}
		
		return '/images/icons/'.$icon.'.png';
	}
	
	public function apiError($errorCode="")
	{
		if ($errorCode != "" && array_key_exists($errorCode, $this->_apiErrors))
		{
			return $this->_apiErrors[$errorCode];
		}
		else
		{
			return "Unknown error";
		}
	}

	public function error($error="", $message="")
	{
		if ($error == "")
		{
			$message = $this->apiError();
		}

		if ($message == "")
		{
			if (property_exists($error, 'code'))
			{
				$message = $this->apiError($error->code);
			}
			elseif (property_exists($error, 'message'))
			{
				$message = $this->apiError($error->message);
			}
		}

		$return = new stdClass();

		$return->error = $message;

		return $return;
	}

	// public function success($data)
	// {
	// 	$return = new stdClass();

	// 	$return->success = $data;

	// 	return $return;
	// }
	
	private function requestApi($apiEndpoint, $params=[])
	{
		if ($this->_agent != "")
		{
			$params["agent"] = $this->_agent;
		}

		if ($this->_token != "")
		{
			$params["token"] = $this->_token;
		}

		if (count($params) > 0)
		{
			$apiEndpoint .= "?" . http_build_query($params);
		}

		// echo 'apiEndpoint: '.$apiEndpoint.'<br />';

		$context = stream_context_create([ 'http' => [ 'ignore_errors' => true ] ]);

		$content = file_get_contents($apiEndpoint, false, $context);

		// echo 'content: '.$content.'<br />';

		$data = json_decode($content);

		/*
		echo '<pre>';
		print_r($data);
		echo '</pre>';
		//*/
		
		if (property_exists($data, 'status'))
		{
			if ($data->status == "error")
			{
				if (property_exists($data, 'error'))
				{
					return $this->error($data->error);
				}

				return $this->error($data->error);
			}
			elseif ($data->status == "success")
			{
				if (property_exists($data, 'data'))
				{
					return $data->data;
				}
			}
			
		}

		return $this->error();
	}
    
	### USER ###
	
    public function login()
	{
		$this->session_reader();
        
        //echo "USERNAME: ".$this->_userInfo->username."\n";

		if ($this->_userInfo->username == '')
		{
			$data = $this->requestApi($this->_apiUrl . "/user");

			//print_r($data);

			if (!$data)
			{
				$this->_userInfo = '';
				$this->session_writer();

				return false;
			}
			else
			{
				$this->_userInfo = $data->user;
				$this->session_writer();

				return true;
			}
		}
		else
		{
			return true;
		}
	}
	
	public function get_userInfo()
	{
		return $this->_userInfo;
	}
	
	public function get_userHosts($hostsOnly="")
	{
		$params = [];

		if ($hostsOnly != "")
		{
			$params["hostsOnly"] = $hostsOnly;
		}

		if ($this->login())
		{
			return $this->requestApi($this->_apiUrl . "/user/hosts", $params);
		}
	}
	
	// public function get_userTorrents()
	// {
	// 	if ($this->login())
	// 	{
	// 		return $this->requestApi($this->_apiUrl . "/torrents");
	// 	}
	// 	else
	// 	{
	// 		$return = new stdClass();
			
	// 		$return->error = "noLogged";
			
	// 		return $return;
	// 	}
	// }

	### HOSTS ###
	
	public function get_supportedHosts($hostsOnly="")
	{
		$params = [];

		if ($hostsOnly != "")
		{
			$params["hostsOnly"] = $hostsOnly;
		}

		return $this->requestApi($this->_apiUrl . "/hosts", $params);
	}
    
    public function get_domainsHosts()
	{
		return $this->requestApi($this->_apiUrl . "/hosts/domains");
	}

	public function get_domainsPriority()
	{
		return $this->requestApi($this->_apiUrl . "/hosts/priority");
	}
	
	### LINKS ###
	
	public function get_singleLinkInfo($link, $password="")
	{
		$params = ["link" => []];
		$params["link"][] = $link;

		if ($password != "")
		{
			$params["password"] = $password;
		}

		if ($this->login())
		{
			return $this->requestApi($this->_apiUrl . "/link/infos", $params);
		}
	}
	
	public function get_multipleLinksInfo($links, $password="")
	{
		$params = ["link" => []];

		foreach ($links as $link)
		{
			$params["link"][] = $link;
		}

		if ($password != "")
		{
			$params["password"] = $password;
		}
		
		if ($this->login())
		{
			return $this->requestApi($this->_apiUrl . "/link/infos", $params);
		}
	}
	
	public function get_redirector($link)
	{
		$params = [];
		$params["link"] = $link;

		if ($this->login())
		{
			return $this->requestApi($this->_apiUrl . "/link/redirector", $params);
		}
	}
	
	public function get_debridLink($link)
	{
		$params = [];
		$params["link"] = $link;

		if ($this->login())
		{
			return $this->requestApi($this->_apiUrl . "/link/unlock", $params);
		}
	}
}