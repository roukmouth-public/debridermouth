allDebridController = '/connectors/alldebrid.php';

function connexion_debrider()
{
	debrider_connection_ko();
	
	$('#img_conn_allDebrid').html(icon_loading);
	
	$.ajax(
	{
		type: 'POST',
		url: allDebridController,
		data: { 'mode': 'debrider_connection' },
		async: false,
		success: function(conn)
		{
			if (conn)
			{
				$.ajax(
				{
					type: 'GET',
					url: allDebridController,
					data: { 'mode': 'debrider_userinfo' },
					async: false,
					dataType: 'json',
					success: function(info)
					{
						//alert(info);
						
						if (info == 'login fail')
						{
							debrider_connection_ko();
						}
						else
						{
							$('#img_conn_allDebrid').html(icon_ok);

							$('#b_debrid').removeAttr('disabled');
							
							var dd = "<dl class='dl-horizontal'>";
							dd += "<dt>Nom d'utilisateur</dt><dd>"+ info.username +"</dd>";
							dd += "<dt>eMail</dt><dd>"+ info.email +"</dd>";
							dd += "<dt>Type de compte</dt><dd>"+ info.type +"</dd>";
							
							if (info.type == 'premium')
							{
								dd += "<dt>Date d'expiration</dt><dd>"+ info.premiumUntil +"</dd>";
							}
							
							dd += '</dl>';
							
							$('#debrid_userinfo').html(dd);

							conn_debrider = true;
						}
					},
					error: function()
					{
						debrider_connection_ko();
					}
				});
			}
		},
		error: function()
		{
			debrider_connection_ko();
		}
	});
}

function debridLink(index, url)
{
	$.post(allDebridController, { 'mode': 'debrider_link', 'link': url }, function(debrid)
	{
		if (debrid.error == 'noLogged')
		{
			tr_link({
				"index": index,
				"classe": "warning",
				"icon": icon_ko,
				"txt": "Vous n'&ecirc;tes plus connect&eacute; &agrave; AllDebrid.",
				"action": "<a href='#' class='btn btn-default b_debrid_again' title=\"Se reconnecter\">"+ icon_reconnection +"</a>"
			});
			
			debrider_connection_ko();
			
			$('.b_debrid_again').unbind('click').click(function()
			{
				var b = $(this);
				var tr = b.closest('tr');
				var index = tr.data('index');
				var url = tr.find('.td_link_url').text();

				//alert(index +' : '+ url);

				tr_link({ "index": index, "classe": "", "icon": icon_loading, "txt": "D&eacute;bridage en cours..." });
				connexion_debrider();
				debridLink(index, url);

				return false;
			});
		}
		else
		{
			if (typeof(debrid.error) == 'undefined')
			{
				tr_link({ "index": index, "classe": "danger", "icon": icon_ko, "txt": "Une erreur inconnue est survenue lors du d&eacute;bridage de ce lien." });
			}
			else if (debrid.error !== '')
			{
				tr_link({ "index": index, "classe": "danger", "icon": icon_ko, "txt": debrid.error });
			}
			else if (debrid.error === '')
			{
				var filesize = '';
				
				if (debrid.filesize !== '')
				{
					filesize = filesize_fr(debrid.filesize);
				}
				
				var action = "<a href='#' class='btn btn-default b_download' title=\"T&eacute;l&eacute;charger\" filename=\""+ debrid.filename +"\" link=\""+ debrid.link +"\">";
				action += "<span class='glyphicon glyphicon-download'></span>";
				action += "</a>";

				var obj = {
					"index": index,
					"classe": "success",
					"icon": "<img src=\""+ debrid.icon +"\" title=\""+ debrid.host +"\" />",
					"url": debrid.link,
					"txt": "<a href=\""+ debrid.link +"\">"+ debrid.filename +"</a>",
					"action": action,
					"filesize": filesize,
					"streaming": debrid.streaming
				};
				
				tr_link(obj);
				
				$('#txt_links_debrid').val($('#txt_links_debrid').val() + debrid.link + "\n");
					
				$('.b_download').unbind('click').click(function(e)
				{
					e.preventDefault();

					var b = $(this);
					var filename = b.attr('filename');
					var link = b.attr('link');
					var links = [];

					links[0] = link;

					pyload_addLink(links, filename);
                });
			}
		}
	}, 'json');
}

function debrid_hostersList()
{
	var result = {};
	
	$.ajax(
	{
		type: 'GET',
		url: allDebridController,
		data: { 'mode': 'debrider_hosters' },
		async: false,
		dataType: 'json',
		success: function(list)
		{
			result = list;
		},
		error: function()
		{
			result = { "error": "Une erreur est survenue lors du chargement des founisseurs" };
		}
	});
	
	return result;
}