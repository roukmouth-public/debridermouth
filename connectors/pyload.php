<?php

include_once('../config/config.inc.php');

define ('_PYLOAD_CONNEXION', $CONFIG['pyload_api'].'login');
define ('_PYLOAD_ADDPACKAGE', $CONFIG['pyload_api'].'addPackage');
define ('_PYLOAD_USERAGENT', "Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2) Gecko/20100115 Firefox/3.6 GTB6");
define ('_PYLOAD_COOKIE_FILENAME', __DIR__ . '/cookies/cookie_pyload.txt');
define ('_PYLOAD_SESSION_FILENAME', __DIR__ . '/cookies/session_pyload.txt');

function pyload_enleveAccent($str, $charset = 'utf-8')
{
	$str = htmlentities($str, ENT_NOQUOTES, $charset);
 
	$str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
	$str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
	$str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
 
	return $str;
}

$ch = curl_init();

// extra headers
$headers[] = "Accept: */*";
$headers[] = "Connection: Keep-Alive";

curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

if ($_POST['mode'] == 'pyload_connect')
{
	$postfields = "username=".urlencode($CONFIG['pyload_user']);
	$postfields .= "&password=".urlencode($CONFIG['pyload_pass']);
	
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	
	curl_setopt($ch, CURLOPT_URL, _PYLOAD_CONNEXION);
	
	//curl_setopt($ch, CURLOPT_COOKIEJAR, _PYLOAD_COOKIE_FILENAME);
	//curl_setopt($ch, CURLOPT_COOKIEFILE, _PYLOAD_COOKIE_FILENAME);
	
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
	
	$session = json_decode($output);
	
	$fp = fopen(_PYLOAD_SESSION_FILENAME, 'w');
	fwrite($fp, $session);
	fclose($fp);
	
	if (preg_match("#^[0-9a-z]{32}$#i", $session))
	{
		echo "CONNECTION_OK";
	}
	else
	{
		echo "CONNECTION_KO";
	}
	
	exit;
}

if ($_POST['mode'] == 'pyload_addPackage')
{
	$packageName = $_POST['packageName'];
	
	if ($packageName == '')
	{
		$packageName = date('YmdHis');
	}
	
	$fp = fopen(_PYLOAD_SESSION_FILENAME, 'r');
	$session = fgets($fp);
	fclose($fp);
	
	$links = '';
	
	foreach ($_POST['links'] as $link)
	{
		if ($link != '')
		{
			if ($links != '')
			{
				$links .= ',';
			}
			
			$links .= "\"$link\"";
		}
	}
	
	$postfields = 'session='.$session;
	//$postfields .= '&name='.urlencode('"'.$packageName.'"');
	$postfields .= '&name="'.pyload_enleveAccent($packageName).'"';
	//$postfields .= '&links=['.urlencode($links).']';
	$postfields .= '&links=['.pyload_enleveAccent($links).']';
	
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	
	curl_setopt($ch, CURLOPT_URL, _PYLOAD_ADDPACKAGE);
	
	//curl_setopt($ch, CURLOPT_COOKIEJAR, _PYLOAD_COOKIE_FILENAME);
	//curl_setopt($ch, CURLOPT_COOKIEFILE, _PYLOAD_COOKIE_FILENAME);
	
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
	
	if ($info['http_code'] == 200)
	{
		echo $output;
	}
	else
	{
		echo "Une erreur est survenue lors de l'ajout du package dans pyLoad.";
		
		//echo '<pre>'; print_r($info); echo '<br />'._PYLOAD_ADDPACKAGE.'?'.$postfields.'</pre>';
	}
	
	exit;
}

?>