<?php

include_once(__DIR__ . '/alldebrid.class.php');

if (in_array($_POST['mode'], array('debrider_connection', 'debrider_link')) || in_array($_GET['mode'], array('debrider_userinfo', 'debrider_hosters')))
{
	$debrider = new AllDebrid($CONFIG['debrid_token']);

	// Récupération des hosters
	if ($_GET['mode'] == 'debrider_hosters')
	{
		$hosters = $debrider->get_userHosts();
		
		/*
		echo '<pre>';
		print_r($hosters);
		echo '</pre>';
		//*/
		
		$output = '';
		
		foreach ($hosters->hosts as $name => $hoster)
		{
			$limitSimuDl = 0;
			$status = $hoster->status;
			
			if (!$status)
			{
				$status = 0;
			}
			
			if (property_exists($hoster, 'limitSimuDl'))
			{
				$limitSimuDl .= $hoster->limitSimuDl;
			}
			
			if ($output != '')
			{
				$output .= ',';
			}
			
			$output .= '{ "name": "'.$name.'", "icon": "'.$debrider->icon($name).'", "url": "http://'.$hoster->domain.'", "status": '.$status.', "limitSimuDl": "'.$limitSimuDl.'"';
			
			if (property_exists($hoster, 'quota') && $hoster->quota != '')
			{
				$output .= ', "quota": "'.$hoster->quota.'", "quotaType": "'.$hoster->quotaType.'"';
			}
		
			$output .= ' }';
		}
		
		echo '['.$output.']';
	}
	// Récupération des informations de l'utilisateur
	elseif ($_GET['mode'] == 'debrider_userinfo')
	{
		$infos = $debrider->get_userInfo();

		// print_r($infos);
		
		if ($infos->isPremium)
		{
			$premium = '"premium", "premiumUntil": "'.date('d.m.Y H:i:s', $infos->premiumUntil).'"';
		}
		else
		{
			$premium = '"normal"';
		}
		
		echo '{ "username": "'.$infos->username.'", "email": "'.$infos->email.'" , "type": '.$premium.' }';
	}
	// Connexion au débrideur
	elseif ($_POST['mode'] == 'debrider_connection')
	{
		echo $debrider->login();
	}
	// Débridage du lien
	elseif ($_POST['mode'] == 'debrider_link')
	{
		$link = $debrider->get_debridLink($_POST['link']);

		/*
		echo '<pre>';
		print_r($link);
		echo '</pre>';
		//*/
		
		if (property_exists($link, 'error') && $link->error != '')
		{
			echo '{ "error": "'.$link->error.'" }';
		}
		else
		{
			echo '{
				"error": "",
				"link": "'.$link->link.'",
				"host": "'.$link->host.'",
				"filename": "'.$link->filename.'",
				"filesize": "'.$link->filesize.'",
				"icon": "'.$debrider->icon($link->host).'"
			}';
		}
	}
}
else
{
	echo '{ "error": "Qu\'est-ce que vous faites l&agrave; ??" }';
}