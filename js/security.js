$(function()
{
   
	$.ajax({
		type: 'GET',
		url: 'config/config.json',
		dataType: 'json',
		success: function(config)
		{
			$('body > .container').prepend("<div class='alert alert-danger'><i class='fa fa-warning' aria-hidden='true'></i> Attention, votre fichier de configuration est accessible par tous. Veuillez le prot&eacute;ger.</div>");
		}
	});
    
});    