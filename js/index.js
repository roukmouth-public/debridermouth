conn_debrider = false;
conn_downloader = false;
icon_ko = "<i class='fa fa-times text-danger' aria-hidden='true'></i>";
icon_ok = "<i class='fa fa-check text-success' aria-hidden='true'></i>";
icon_reconnection = "<i class='fa fa-bolt' aria-hidden='true'></i>";
icon_loading = "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i>";

function debrider_connection_ko()
{
	conn_debrider = false;
	
	$('#img_conn_allDebrid').html(icon_ko);
	$('#b_debrid').attr('disabled', 'disabled');
	$('#debrid_info').html("<div class='alert alert-danger'><i class='fa fa-times'></i> Vous n'&ecirc;tes actuellement pas connect&eacute; au debrider.</div>");
}

function downloader_connection_ko()
{
	conn_downloader = false;
	
	$('#b_allDownload').attr('disabled', 'disabled');
    $('#img_conn_pyload').html(icon_ko);
}

function filesize_fr(bytes, decimals)
	{
	if (typeof(bytes) != 'undefined' && bytes !== '')
		{
		if (bytes === 0) return '0 Octet';
		
		var k = 1000;
		var dm = decimals + 1 || 3;
		var sizes = ['Octets', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo'];
		var i = Math.floor(Math.log(bytes) / Math.log(k));
		
		return (bytes / Math.pow(k, i)).toPrecision(dm) + ' ' + sizes[i];
		}
	else
		{
		return '';
		}
	}

function tr_link(obj)
	{
	$('#tr_link_'+ obj.index).removeClass('danger success warning');
	$('#tr_link_'+ obj.index +' .td_link_icon').html('');
	$('#tr_link_'+ obj.index +' .td_link_txt').html('');
	$('#tr_link_'+ obj.index +' .td_link_action').html('');
		
	if (obj.classe !== '')
		{
		$('#tr_link_'+ obj.index).addClass(obj.classe);
		}
	
	if (obj.icon !== '')
		{
		$('#tr_link_'+ obj.index +' .td_link_icon').html(obj.icon);
		}
	
	if (obj.url !== '')
		{
		$('#tr_link_'+ obj.index +' .td_link_url').html(obj.url);
		}
	
	if (obj.txt !== '')
		{
		$('#tr_link_'+ obj.index +' .td_link_txt').html(obj.txt);
		}
	
	if (obj.action !== '')
		{
		$('#tr_link_'+ obj.index +' .td_link_action').html(obj.action);
		}
	
	if (typeof(obj.filesize) != 'undefined' && obj.filesize !== '')
		{
		$('#tr_link_'+ obj.index +' .td_link_txt').append(' ('+ obj.filesize +')');
		}
	
	$.each(obj.streaming, function(index, value)
		{
		$('#tr_link_'+ obj.index +' .td_link_txt').append(' [<a href="'+ value +'" target="_blank">'+ index +'</a>]');
		});
	}

function modal_divers(settings)
	{
	$('#modal_divers .modal-title').html('');
	$('#modal_divers .modal-body').html('');
	$('#modal_divers .modal-footer').html('');
	
	if (settings.body)
		{
		if (settings.title)
			{
			$('#modal_divers .modal-title').html(settings.title);
			}

		if (settings.footer)
			{
			$('#modal_divers .modal-footer').html(settings.footer);
			}
		
		$('#modal_divers .modal-body').html(settings.body);
		
		if (typeof(settings.onAfterLoad) == 'function')
			{
			settings.onAfterLoad();
			}
		
		$('#modal_divers').modal('show');
		}
	}

function prefixPackageRemove()
	{
	$('.prefixPackageRemove').unbind('click').click(function()
		{
		var b = $(this);
		var li = b.closest('li');
		
		li.remove();
		});
	}

function load_debrid_hosters()
{
	$('#debrid_hostersList table tbody').html("<tr><td colspan='100%' class='text-center'>"+ icon_loading +" Chargement en cours...</td></tr>");
		
	var list = debrid_hostersList();
	
	if (typeof(list.error) != 'undefined' && list.error !== '')
	{
		$('#debrid_hostersList table tbody').html("<tr><td colspan='100%' class='text-center'>" + list.error + "</td></tr>");
	}
	else
	{
		var tr = '';
		
		for (var i = 0; i < list.length; i++)
		{
			var quota = '';
			var limitSimuDl = 'Illimit&eacute;';
			var status = icon_ok;
			
			if (typeof(list[i].quota) != 'undefined')
			{
				quota = list[i].quota;
			
				if (list[i].quotaType == 'traffic')
				{
					quota += ' Mo';
				}
				else if (list[i].quotaType == 'nb_download')
				{
					quota += ' dl';
				}
			}
			
			if (list[i].limitSimuDl > 0)
			{
				limitSimuDl = list[i].limitSimuDl;
			}
			
			if (!list[i].status)
			{
				status = icon_ko;
			}
			
			tr += "<tr>";
			tr += "<td class='text-center'><img src='" + list[i].icon + "' title=\""+ list[i].name +"\" /></td>";
			tr += "<td><a href='" + list[i].url + "' target='_blank'>"+ list[i].name +"</a></td>";
			tr += "<td class='text-center'>" + quota + "</td>";
			tr += "<td class='text-center'>" + limitSimuDl + "</td>";
			tr += "<td class='text-center'>" + status + "</td>";
			tr += "</tr>";
		}
		
		$('#debrid_hostersList table tbody').html(tr);
	}
}

$(function()
	{

	$('#b_vider').click(function()
		{
		$('#txt_links').val('');
		
		return false;
		});

	$('#b_debrid').click(function()
		{
		$('#tab_result tbody').html('');
		$('#txt_links_debrid, #t_packageName').val('');
		$('#sect_tab_result, #sect_links_debrid').hide();
		
		if (!conn_debrider)
			{
			connexion_debrider();
			}
	
		var arrayOfLines = $('#txt_links').val().split('\n');
		
		if (arrayOfLines.length > 0 && $('#txt_links').val() !== '')
			{
			$('#sect_tab_result, #sect_links_debrid').show();
			
			$.each(arrayOfLines, function(index, url)
				{
				if (url !== '')
					{
					var tr = "<tr id='tr_link_"+ index +"' data-index='"+ index +"'>";
					tr += "<td class='td_link_icon'>";
					tr += icon_loading;
					tr += "</td>";
					tr += "<td class='td_link_url'>"+ url +"</td>";
					tr += "<td class='td_link_txt'>D&eacute;bridage en cours...</td>";
					tr += "<td class='td_link_action'></td>";
					tr += "</tr>";
					
					$('#tab_result tbody').append(tr);

					debridLink(index, url);
					}
				});
			}
		
		return false;
		});

	$('#b_allDownload').click(function()
		{
		if (!conn_downloader)
			{
			connexion_downloader();
			}
		
		if ($('#t_packageName').val() === '')
			{
			var settings = {};
			
			settings.title = "Nom de package invalide";
			settings.body = "<div class='alert alert-danger'><span class='glyphicon glyphicon-remove'></span> Veuillez renseigner un nom de package pour pyLoad.</div>";
			settings.footer = "<button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>";
			
			modal_divers(settings);
			}
		else
			{
			var arrayOfLines = $('#txt_links_debrid').val().split('\n');
			var packageName = $('#s_prefixPackage').val() + $('#t_packageName').val();
			
			downloader_addLink(arrayOfLines, packageName);
			}
		
		return false;
		});
	
	$('#b_config').click(function()
		{
		$.get('config/configForm.php', function(form)
			{
			var settings = {};
			
			settings.title = "Configuration";
			settings.body = form;
			settings.onAfterLoad = function()
				{
				prefixPackageRemove();
				
				$('#b_newPrefixPackage').click(function()
					{
					var newPrefix = $('#t_newPrefixPackage').val();

					var li = "<li>";
					li += "<input type='hidden' name='prefixPackage[]' value=\""+ newPrefix +"\" />";
					li += "<i class='fa fa-trash text-danger clickable prefixPackageRemove' aria-hidden='true'></i> ";
					li += newPrefix;
					li += "</li>";
					
					$('#listPrefixPackage').append(li);
					$('#t_newPrefixPackage').val('').focus();
					
					prefixPackageRemove();
					});
				}
			
			modal_divers(settings);
			});
		});
	
	$('#b_debrid_connection').click(function()
		{
		connexion_debrider();
		
		//return false;
		});
	
	$('#b_debrid_userinfo').click(function()
		{
		var settings = {};
		
		settings.title = "Information du compte";
		settings.body = $('#debrid_userinfo').html();
		settings.footer = "<button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>";
		
		modal_divers(settings);
		
		//return false;
		});
	
	$('#b_pyload_connection').click(function()
		{
		connexion_downloader();
		
		//return false;
		});
	
	$('#b_list_hosters').click(function()
		{
		var settings = {};

		settings.title = "Liste des fournisseurs";
		settings.body = $('#debrid_hostersList').html();
		settings.footer = "<button type='button' class='btn btn-default' data-dismiss='modal'>Fermer</button>";

		modal_divers(settings);
		});
	
	connexion_debrider();
	connexion_downloader();
	load_debrid_hosters();
	
	});